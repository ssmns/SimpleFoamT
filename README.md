## SimpleFoamT for openFoam7
```bash
mkdir -p $FOAM_RUN 
mkdir -p $FOAM_USER_APPBIN
```

```bash
cd /home/$USER/OpenFOAM/$USER-7
```
```bash
# ssh
git clone git@codeberg.org:ssmns/SimpleFoamT.git
# http:
# git clone https://codeberg.org/ssmns/SimpleFoamT.git
# zip:
# wget https://codeberg.org/ssmns/SimpleFoamT/archive/master.zip
```


```bash
cd SimpleFoamT
sudo su 
. /opt/openfoam7/etc/bashrc
wmake 
```